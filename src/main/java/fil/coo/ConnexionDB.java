package fil.coo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionDB {

    private static ConnexionDB ourInstance = new ConnexionDB();

    public static ConnexionDB getInstance() {
        return ourInstance;
    }

    private Connection connection;

    private ConnexionDB() {

    }
    private Connection connect(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection("jdbc:oracle:thin:@oracle.fil.univ-lille1.fr:1521:filora", "vandeputte","e94a0dc724");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }
    public Connection getConnection(){
        try {
            return (connection == null || connection.isClosed()) ? connect() : connection;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

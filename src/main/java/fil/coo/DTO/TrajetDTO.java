package fil.coo.DTO;

import fil.coo.Display.Conducteur;
import fil.coo.Display.Voyageur;

import java.util.List;

public class TrajetDTO {
    private String lieuDepart;
    private String lieuArrive;
    private String dateDepart;
    private int tarif;
    private int idTrajet;
    private Conducteur c;
    private List<Voyageur> l;

    public TrajetDTO(){

    }

    public TrajetDTO(String lieuDepart, String lieuArrive, String dateDepart, int tarif, int idTrajet) {
        this.lieuDepart = lieuDepart;
        this.lieuArrive = lieuArrive;
        this.dateDepart = dateDepart;
        this.tarif = tarif;
        this.idTrajet = idTrajet;
    }

    public String getLieuDepart() {
        return lieuDepart;
    }

    public void setLieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
    }

    public String getLieuArrive() {
        return lieuArrive;
    }

    public void setLieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
    }

    public String getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }

    public int getTarif() {
        return tarif;
    }

    public void setTarif(int tarif) {
        this.tarif = tarif;
    }

    public int getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(int idTrajet) {
        this.idTrajet = idTrajet;
    }

    public void setConducteur(Conducteur c) {
    }

    public void setVoyageur(List<Voyageur> l) {
    }
}

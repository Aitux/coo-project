package fil.coo.DTO;

import fil.coo.domain.VehiculeDAO;

public class UtilisateurDTO {
    private String nom;
    private String prenom;
    private int age;
    private String login;
    private VehiculeDAO VehiculeDAO;
    private int idUtilisateur;

    public UtilisateurDTO(){

    }

    public UtilisateurDTO(String nom, String prenom, int age, String login, VehiculeDAO vehiculeDAO, int idUtilisateur) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.login = login;
        VehiculeDAO = vehiculeDAO;
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public VehiculeDAO getVehiculeDAO() {
        return VehiculeDAO;
    }

    public void setVehiculeDAO(VehiculeDAO vehiculeDAO) {
        VehiculeDAO = vehiculeDAO;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

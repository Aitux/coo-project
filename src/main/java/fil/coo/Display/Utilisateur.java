package fil.coo.Display;

import fil.coo.DTO.UtilisateurDTO;

public abstract class Utilisateur {

    UtilisateurDTO udto;

    public abstract String display();

    public UtilisateurDTO getDTO(){
        return udto;
    }
}

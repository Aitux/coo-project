package fil.coo.Interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import fil.coo.View;
import fil.coo.controllerViewer.*;

public class Accueil extends  JFrame{
    private JPanel panelAccueil;
    private JButton Register;
    private JButton signUpButton;


    public Accueil() {
        ControllerViewer.getInstance().setView(View.Accueil);
        JFrame frame = new JFrame("Accueil TchitTchatCar");
        frame.setContentPane(panelAccueil);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        Register.addActionListener(actionEvent ->{
            PopUpSignIn p = new PopUpSignIn();
            frame.dispose();

        });
        // + maj View


        signUpButton.addActionListener(actionEvent ->{
            new PopUpSignUp();
            frame.dispose();
        });
    }



}

package fil.coo.Interface;

import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AccueilLogin {
    private JPanel ProfilLogin;
    private JButton modifierMesInfosButton;
    private JButton rechercherUnTrajetButton;
    private JButton proposerUnTrajetButton;
    private JButton historiqueDeMesTrajetsButton;
    private JButton SeDéconnecter;

    public AccueilLogin() {

        ControllerViewer.getInstance().setView(View.AccueilLogin);
        JFrame frame = new JFrame("Menu principal");
        frame.setContentPane(ProfilLogin);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);


        modifierMesInfosButton.addActionListener(actionEvent ->{
            new ModifierInfo();
            frame.dispose();
        });


        rechercherUnTrajetButton.addActionListener(actionEvent -> {
            new RechercherUnTrajet();
            frame.dispose();
        });


        proposerUnTrajetButton.addActionListener(actionEvent -> {
            new ProposerUnTrajet();
            frame.dispose();
        });


        historiqueDeMesTrajetsButton.addActionListener(actionEvent ->{
            new Historique();
            frame.dispose();
        });

        SeDéconnecter.addActionListener(actionEvent -> System.exit(0));
    }
}

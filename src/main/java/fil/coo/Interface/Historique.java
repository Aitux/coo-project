package fil.coo.Interface;

import fil.coo.Service.ServiceTrajet;
import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Historique {
    private JPanel panel1;
    private JButton retourMenu;
    private JTextArea ListeTrajet;

    public Historique(){
        ControllerViewer.getInstance().setView(View.Historique);
        JFrame frame = new JFrame("Historique de mes trajets ");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(panel1);
        frame.pack();
        frame.setVisible(true);

        ServiceTrajet st = new ServiceTrajet();
        st.getTrajetWhereConducteur();
        ListeTrajet.append(st.getDTOWhereIAmConducteur().toString());


        //ServiceTrajet st2 = new ServiceTrajet();
        //st2.getTrajetWhereVoyageur();

        //ListeTrajet.append();



        retourMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                frame.setDefaultCloseOperation(0);
            }
        });
    }
}

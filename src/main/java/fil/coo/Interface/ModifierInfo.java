package fil.coo.Interface;

import fil.coo.Service.ServiceUtilisateur;
import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.domain.UtilisateurDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifierInfo {


    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton validerLesModificationsButton;
    private JPanel ModifierInfo;

    public ModifierInfo(){
        ControllerViewer.getInstance().setView(View.ModifierInfo);
        JFrame frame = new JFrame("Modifier mes informations");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(ModifierInfo);
        frame.pack();
        frame.setVisible(true);


        validerLesModificationsButton.addActionListener(actionEvent -> {
            try{
                String nom = textField1.getText();
                String prenom = textField2.getText();
                int age = Integer.parseInt(textField3.getText()) ;
                UtilisateurDAO ud = ControllerViewer.getInstance().getConnected();
                UtilisateurDAO udbis = new UtilisateurDAO(ud);
                udbis.setAge(age);
                udbis.setPrenom(prenom);
                udbis.setNom(nom);
                ServiceUtilisateur su = new ServiceUtilisateur();
                su.updateUser(ud, udbis);
                JOptionPane.showMessageDialog(null,"Votre profil a été mis à jour !");
                ControllerViewer.getInstance().setConnected(udbis);
                new AccueilLogin();
                frame.dispose();

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Attention, il y a une erreur sur vos données !");
            }


        });
    }

}
package fil.coo.Interface;

import fil.coo.Service.ServiceUtilisateur;
import fil.coo.Service.ServiceVehicule;
import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.domain.UtilisateurDAO;
import fil.coo.domain.VehiculeDAO;

import javax.swing.*;

public class PopUpSignIn {
    private JPanel PopUpSignIn;
    private JButton validerButton;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField1;
    private JTextField textField4;
    private JComboBox<String> comboBox1;
    private JButton annulerButton;

    public PopUpSignIn() {
        ControllerViewer.getInstance().setView(View.PopUpSignIn);
        JFrame frame = new JFrame("S'enregistrer");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ServiceVehicule sv = new ServiceVehicule();
        for (VehiculeDAO v :
                sv.getVehicules()) {
            comboBox1.addItem(v.getMarque());
        }

        frame.setContentPane(PopUpSignIn);
        frame.pack();
        frame.setVisible(true);
        validerButton.addActionListener(actionEvent -> {
            try {
                String login = textField1.getText().trim();
                String nom = textField2.getText().trim();
                String prenom = textField3.getText().trim();
                int age = Integer.parseInt(textField4.getText().trim());
                String marque = (String) comboBox1.getSelectedItem();
                marque = marque.trim();

                UtilisateurDAO ud = new UtilisateurDAO();
                ud.setIdUtilisateur(0);
                ud.setAge(age);
                ud.setLogin(login);
                ud.setNom(nom);
                ud.setPrenom(prenom);
                ServiceUtilisateur su = new ServiceUtilisateur();
                su.insertUtilisateur(ud, marque);
                JOptionPane.showMessageDialog(null, "Vous êtes bien enregistré, vous pouvez maintenant vous identifier avec le login: " + login + " :)");
                new Accueil();
                frame.dispose();

            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Attention, il y a une erreur sur vos données, ou alors votre login est déjà utilisé !");
            }
        });

        annulerButton.addActionListener(actionEvent -> {
                new Accueil();
                frame.dispose();

        });
    }
}

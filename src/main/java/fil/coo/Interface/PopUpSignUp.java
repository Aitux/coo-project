package fil.coo.Interface;

import fil.coo.Service.ServiceUtilisateur;
import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.domain.UtilisateurDAO;

import javax.swing.*;

public class PopUpSignUp {
    private JButton validerButton;
    private JTextField monLoginTextField;
    private JPanel SignUp;
    private JButton AnnulerButton;

    public PopUpSignUp() {
        ControllerViewer.getInstance().setView(View.PopUpSignUp);
        JFrame frame = new JFrame("Se connecter");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(SignUp);
        frame.pack();
        frame.setVisible(true);
        validerButton.addActionListener(actionEvent -> {
            try {
                String login = monLoginTextField.getText();
                ServiceUtilisateur su = new ServiceUtilisateur();
                UtilisateurDAO d = su.getUserFromLogin(login);
                if (d == null)
                    JOptionPane.showMessageDialog(null, "Attention, login erroné ! ");
                else {
                    JOptionPane.showMessageDialog(null, "Vous êtes bien connecté :)");
                    ControllerViewer.getInstance().setConnected(d);
                    new AccueilLogin();
                    frame.dispose();
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Attention, login erroné ! ");
            }
        });

        AnnulerButton.addActionListener(actionEvent -> {
                new Accueil();
                frame.dispose();
        });
    }
}

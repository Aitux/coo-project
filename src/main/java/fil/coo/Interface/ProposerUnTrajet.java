package fil.coo.Interface;

import fil.coo.Service.ServiceTrajet;
import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.domain.TrajetDAO;

import javax.swing.*;

public class ProposerUnTrajet {
    private JPanel ProposerTrajet;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JButton validerEtMettreEnButton;
    private JComboBox comboBox1;

    public ProposerUnTrajet(){
        ControllerViewer.getInstance().setView(View.ProposerUnTrajet);
        JFrame frame = new JFrame("Proposer un Trajet");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(ProposerTrajet);
        frame.pack();
        frame.setVisible(true);
        validerEtMettreEnButton.addActionListener(actionEvent -> {
            String depart = textField1.getText();
            String arrivee = textField2.getText();
            int prix = Integer.parseInt(textField3.getText());
            String date = textField3.getText();
            ServiceTrajet st = new ServiceTrajet();
            TrajetDAO td = new TrajetDAO();
            td.setDateDepart(date);
            td.setIdTrajet(0);
            td.setLieuArrive(arrivee);
            td.setLieuDepart(depart);
            st.proposerTrajet(td, ControllerViewer.getInstance().getConnected());

            JOptionPane.showMessageDialog(null,"Votre trajet est en ligne !");
            new AccueilLogin();
            frame.dispose();
        });
    }
}

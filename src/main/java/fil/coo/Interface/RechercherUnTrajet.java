package fil.coo.Interface;

import fil.coo.View;
import fil.coo.controllerViewer.ControllerViewer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RechercherUnTrajet {
    private JPanel RechercherTrajet;
    private JButton rechercherCeTrajetButton;
    private JTextField textField1;
    private JTextField textField2;
    private JComboBox comboBox1;
    private JTextField textField3;


    public RechercherUnTrajet() {
        ControllerViewer.getInstance().setView(View.RechercherUnTrajet);
        JFrame frame = new JFrame("Rechercher un trajet");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setContentPane(RechercherTrajet);
        frame.pack();
        frame.setVisible(true);


        rechercherCeTrajetButton.addActionListener(actionEvent -> {
            String depart = textField1.getText();
            String arrive = textField2.getText();
            int horaire = comboBox1.getSelectedIndex();
            int prix = Integer.parseInt(textField3.getText());

        });
    }
}

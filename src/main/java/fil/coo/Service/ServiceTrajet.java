package fil.coo.Service;

import fil.coo.DTO.TrajetDTO;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.datamapper.ConducteurMapper;
import fil.coo.datamapper.TrajetMapper;
import fil.coo.datamapper.VoyageurMapper;
import fil.coo.domain.ConducteurDAO;
import fil.coo.domain.TrajetDAO;
import fil.coo.domain.UtilisateurDAO;
import fil.coo.domain.VoyageurDAO;

import java.util.ArrayList;
import java.util.List;

public class ServiceTrajet {


    public void proposerTrajet(TrajetDAO t, UtilisateurDAO u){
        TrajetMapper tm = new TrajetMapper();
        tm.create(t);
        // La solution du readOne est.... nulle mais je n'arrive pas à récupérer la clef générée par la séquence :/
        TrajetDAO tra = tm.readOne("select * from COO_TRAJET where lieu_depart='" + t.getLieuDepart() +"' AND lieu_arrive='"+t.getLieuArrive()+"' AND date_depart='" + t.getDateDepart() + "' AND tarif='"+t.getTarif()+"'");
        ConducteurDAO cd = new ConducteurDAO();
        cd.setIdTrajet(tra.getIdTrajet());
        cd.setIdUtilisateur(u.getIdUtilisateur());
    }


    public List<TrajetDAO> getTrajetWhereConducteur(){
        ConducteurMapper um = new ConducteurMapper();
        TrajetMapper tm = new TrajetMapper();
        List<ConducteurDAO> c = um.readMany("SELECT * from COO_CONDUCTEUR where id_utilisateur = "+ ControllerViewer.getInstance().getIDUtilisateur());
        List<TrajetDAO> trajetDAOS = new ArrayList<>();

        for (ConducteurDAO a :
                c) {
            TrajetDAO t = tm.readOne("SELECT * from COO_TRAJET where id_trajet = " +a.getIdTrajet());
            trajetDAOS.add(t);
        }

        return trajetDAOS;
    }

    public List<TrajetDAO> getTrajetWhereVoyageur(){
        VoyageurMapper um2 = new VoyageurMapper();
        TrajetMapper tm2 = new TrajetMapper();
        List<VoyageurDAO> voyageurDAO = um2.readMany("select * from COO_VOYAGEUR where id_utilisateur = "+ ControllerViewer.getInstance().getIDUtilisateur());
        List<TrajetDAO>  trajetvoyageur = new ArrayList<>();

        for (VoyageurDAO v :
                voyageurDAO) {
            TrajetDAO trajetDAO2 = tm2.readOne("SELECT * from COO_TRAJET where id_trajet = " + v.getIdTrajet());
            trajetvoyageur.add(trajetDAO2);
        }

        return trajetvoyageur;
    }

    public List<TrajetDTO> getDTOWhereIAmConducteur(){
        List<TrajetDAO> daos = getTrajetWhereConducteur();
        List<TrajetDTO> res = new ArrayList<>();
        TrajetDTO tdto;
        for(TrajetDAO td : daos){
            tdto = createDTOTrajet(td);
            res.add(tdto);
        }
        return res;
    }

    private TrajetDTO createDTOTrajet(TrajetDAO t){
        TrajetDTO dtotrajet = new TrajetDTO();
        dtotrajet.setDateDepart(t.getDateDepart());
        dtotrajet.setIdTrajet(t.getIdTrajet());
        dtotrajet.setLieuDepart(t.getLieuDepart());
        dtotrajet.setLieuArrive(t.getLieuDepart());
        dtotrajet.setTarif(t.getTarif());
//        dtotrajet.setConducteur(getTrajetWhereConducteur());
//        dtotrajet.setVoyageur(getTrajetWhereVoyageur());


        return dtotrajet ;
    }
}

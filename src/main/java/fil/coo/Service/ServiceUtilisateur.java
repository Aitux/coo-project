package fil.coo.Service;

import fil.coo.DTO.UtilisateurDTO;
import fil.coo.controllerViewer.ControllerViewer;
import fil.coo.datamapper.PossedeMapper;
import fil.coo.datamapper.UtilisateurMapper;
import fil.coo.datamapper.VehiculeMapper;
import fil.coo.domain.PossedeDAO;
import fil.coo.domain.UtilisateurDAO;
import fil.coo.domain.VehiculeDAO;

public class ServiceUtilisateur {

    public VehiculeDAO getVehicule(){
        VehiculeMapper vm = new VehiculeMapper();
        return vm.readOne("SELECT * from COO_POSSEDE where id_utilisateur = "+ ControllerViewer.getInstance().getIDUtilisateur());
    }


    public static void main(String[] args){
        ServiceUtilisateur su = new ServiceUtilisateur();
    }

    public UtilisateurDAO getUserFromLogin(String login) {
        UtilisateurMapper um = new UtilisateurMapper();
        return um.readOne("SELECT * from COO_UTILISATEUR WHERE login='"+login+"'");
    }

    public void insertUtilisateur(UtilisateurDAO u, String marque){
        VehiculeMapper vm = new VehiculeMapper();
        VehiculeDAO v = vm.readOne("select * from COO_Vehicule where marque='" + marque+"'");
        UtilisateurMapper um = new UtilisateurMapper();
        um.create(u);
        UtilisateurDAO a = um.readOne("select * from COO_Utilisateur where login='"+u.getLogin()+"'");
        System.out.println(a.getIdUtilisateur());
        PossedeDAO p = new PossedeDAO(a.getIdUtilisateur(), v.getIdVehicule());
        System.out.println(p.toString());
        PossedeMapper pm = new PossedeMapper();
        pm.create(p);
    }

    public UtilisateurDTO createDTOUtilisateur(){
        UtilisateurDTO dto = new UtilisateurDTO();
        UtilisateurDAO u = ControllerViewer.getInstance().getConnected();
        dto.setAge(u.getAge());
        dto.setIdUtilisateur(u.getIdUtilisateur());
        dto.setLogin(u.getLogin());
        dto.setNom(u.getNom());
        dto.setPrenom(u.getPrenom());
        //dto.setVehicule(getVehicule());
        return dto;
    }

    public void updateUser(UtilisateurDAO ud, UtilisateurDAO udbis) {
        UtilisateurMapper um = new UtilisateurMapper();
        um.update(ud,udbis);
    }
}

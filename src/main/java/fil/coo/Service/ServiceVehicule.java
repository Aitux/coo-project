package fil.coo.Service;

import fil.coo.datamapper.VehiculeMapper;
import fil.coo.domain.VehiculeDAO;

import java.util.List;

public class ServiceVehicule {

    public List<VehiculeDAO> getVehicules(){
        List<VehiculeDAO> vehicule;
        VehiculeMapper vm = new VehiculeMapper();
        vehicule = vm.readMany("select * from COO_Vehicule");
        return vehicule;
    }


}

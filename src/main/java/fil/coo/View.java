package fil.coo;

public enum View {
    Accueil, AccueilLogin, Historique, ModifierInfo, PopUpSignIn, PopUpSignUp, ProposerUnTrajet, RechercherUnTrajet
}


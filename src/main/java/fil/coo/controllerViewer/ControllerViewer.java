package fil.coo.controllerViewer;

import fil.coo.View;
import fil.coo.domain.UtilisateurDAO;

public class ControllerViewer {

    private static ControllerViewer controllerViewer = new ControllerViewer();
    private UtilisateurDAO connected;
    private View v;
    private int IdUtilisateur;

    public UtilisateurDAO getConnected() {
        return connected;
    }

    public void setConnected(UtilisateurDAO connected) {
        this.connected = connected;
        this.IdUtilisateur = connected.getIdUtilisateur();
    }

    private ControllerViewer() {

    }

    public static ControllerViewer getInstance() {
        return controllerViewer;
    }

    public int getIDUtilisateur() {
        //récupère l'id de l'utilisateur connecté
        return 0;
    }

    public void setView(View v) {
        this.v = v;
    }
}


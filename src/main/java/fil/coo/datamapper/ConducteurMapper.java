package fil.coo.datamapper;

import fil.coo.domain.ConducteurDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ConducteurMapper extends DataMapper<ConducteurDAO> {

    public ConducteurMapper() {
        super(new ConducteurDAO());
    }


    @Override
    protected ConducteurDAO initObject(ResultSet rs) {
        ConducteurDAO c = new ConducteurDAO();
        try {
            c.setIdTrajet(rs.getInt("id_trajet"));
            c.setIdUtilisateur(rs.getInt("id_utilisateur"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return c;
    }
}

package fil.coo.datamapper;

import fil.coo.ConnexionDB;
import fil.coo.domain.TrajetDAO;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

public abstract class DataMapper<T> {

    private String tableName;
    private List<String> nomChampBdd;
    private List<Method> getter;

    public DataMapper(T t) {

        Class<?> c = t.getClass();
        Method[] m = c.getMethods();
        tableName = "COO_" + c.getSimpleName().substring(0, c.getSimpleName().length() - 3);
        // On stream les méthodes pour ne garder que les getters et les setters pour pouvoir retrouver les attributs de la classe malgré qu'ils soient en privé
        List<Method> mlist = Arrays.stream(m).filter(x -> x.getName().startsWith("get")).filter(x -> !x.getName().equals("getClass")).collect(Collectors.toCollection(ArrayList::new));
        getter = new ArrayList<>();
        getter.addAll(mlist);
        mlist.addAll(Arrays.stream(m).filter(x -> x.getName().startsWith("set")).collect(Collectors.toCollection(ArrayList::new)));

        List<String> str = parseName(mlist);
        nomChampBdd = str;
        nomChampBdd.sort(String::compareTo);
        getter.sort(Comparator.comparing(Method::getName));
    }

        public static void main(String[] args) {
        TrajetMapper tm = new TrajetMapper();
//        System.out.println(tm.getTableName());
//        tm.getNomChampBdd().forEach(System.out::println);
//        tm.create(new TrajetDAO("Lille", "Marseille", "1500", 15, 3));
//       System.out.println(tm.associate().toString());
        tm.update(new TrajetDAO("Lille", "Paris", "1600", 15, 3),new TrajetDAO("Lille", "Marseille", "1500", 15, 3));
    }

    private List<String> parseName(List<Method> str) {
        Set<String> res = new HashSet<>();
        for (Method ms :
                str) {
            String s = ms.getName();
            StringBuilder champs = new StringBuilder();
            for (int i = 3; i < s.length(); i++) {
                char c = s.charAt(i);
                if (i == 3 && Character.isUpperCase(c)) {
                    champs.append(Character.toLowerCase(c));
                } else if (Character.isUpperCase(c)) {
                    champs.append("_").append(Character.toLowerCase(c));
                } else {
                    champs.append(c);
                }
            }
            res.add(champs.toString());
        }
        List<String> finalRes = Arrays.asList(res.toArray(new String[]{""}));
        return finalRes;
    }

    public boolean create(T t) {
        Connection c = ConnexionDB.getInstance().getConnection();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO ").append(getTableName()).append("(");
        for (String str :
                nomChampBdd) {
            stringBuilder.append(str).append(", ");
        }
        stringBuilder.replace(0, stringBuilder.length(), stringBuilder.substring(0, (stringBuilder.length() - 2)));
        stringBuilder.append(") values (");
        try {
            for (Method met :
                    getter) {
                stringBuilder.append("'").append(met.invoke(t)).append("', ");
            }
            stringBuilder.replace(0, stringBuilder.length(), stringBuilder.substring(0, (stringBuilder.length() - 2)));
            stringBuilder.append(")");

        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return !jsp(c, stringBuilder);


    }

    /**
     * Les paramètres doivent être passé dans l'ordre alphabétique des champs que l'on souhaite insérer en base de données
     * Exemple:
     * <h1>Table Test</h1>
     * <table>
     * <tr>
     * <th>label</th>
     * <th>valeur</th>
     * </tr>
     * <tr>
     * <td>BChamp</td>
     * <td>5</td>
     * </tr>
     * <tr>
     * <td>AChamp</td>
     * <td>'toto'</td>
     * </tr>
     * </table>
     * Ou on aura tendance à passer les paramètres comme suit:
     * <b>create('5','toto');</b>
     * Devra être passé comme ceci:
     * <b>create('toto','5');</b>
     * Car le Mapper tri les champs par ordre aplhabétique pour pouvoir associer une valeur à un champ.
     *
     * @param values des Strings
     * @return true si tous c'est bien passé, false sinon
     */
    @Deprecated(since = "8")
    public boolean create(String... values) {
        // La méthode est expérimentale... à ne pas utiliser dans l'idéal.
        if (values == null)
            return false;
        if (values.length < getter.size())
            return false;
        Connection c = ConnexionDB.getInstance().getConnection();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("INSERT INTO ").append(getTableName()).append("(");
        for (String str :
                nomChampBdd) {
            stringBuilder.append(str).append(", ");
        }


        stringBuilder.replace(0, stringBuilder.length(), stringBuilder.substring(0, (stringBuilder.length() - 2)));
        stringBuilder.append(") values (");
        for (String s :
                values) {
            stringBuilder.append("'").append(s).append("', ");
        }
        stringBuilder.replace(0, stringBuilder.length(), stringBuilder.substring(0, (stringBuilder.length() - 2)));
        stringBuilder.append(");");
        System.out.println(stringBuilder.toString());

        return !jsp(c, stringBuilder);
    }

    public T readOne(String sqlquery) {
        Connection c = ConnexionDB.getInstance().getConnection();
        try {
            PreparedStatement stmt = c.prepareStatement(sqlquery);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                return initObject(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Map<String, Method> associate() {
        Map<String, Method> res = new HashMap<>();
        for (int i = 0; i < nomChampBdd.size(); i++) {
            res.put(nomChampBdd.get(i), getter.get(i));
        }
        return res;
    }

    public boolean update(T before, T after) {
        Connection c = ConnexionDB.getInstance().getConnection();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE ").append(getTableName()).append(" SET ");
        Map<String, Method> methodMap = associate();
        try {
            for (Map.Entry<String, Method> e : methodMap.entrySet()) {

                stringBuilder.append(e.getKey()).append("=").append("'").append(e.getValue().invoke(after)).append("', ");

            }
            stringBuilder.append("WHERE ");
            for (Map.Entry<String, Method> e : methodMap.entrySet()) {

                stringBuilder.append(e.getKey()).append("=").append("'").append(e.getValue().invoke(before)).append("' AND ");

            }
        } catch (IllegalAccessException | InvocationTargetException e1) {
            e1.printStackTrace();
        }

        System.out.println(stringBuilder.toString());
        return false;
    }

    public boolean delete(T t) {
        Connection c = ConnexionDB.getInstance().getConnection();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("DELETE FROM ").append(getTableName()).append(" WHERE ");
        Map<String, Method> methodMap = associate();
        try {
            for (Map.Entry<String, Method> e : methodMap.entrySet()) {
                stringBuilder.append(e.getKey()).append("=").append("'").append(e.getValue().invoke(t)).append("' AND ");
            }
        } catch (IllegalAccessException | InvocationTargetException e1) {
            e1.printStackTrace();
            return false;
        }
        stringBuilder.replace(0, stringBuilder.length(), stringBuilder.substring(0, (stringBuilder.length() - 5)));
        System.out.println(stringBuilder.toString());
        return !jsp(c, stringBuilder);
    }

    public List<T> readMany(String sqlquery) {
        Connection c = ConnexionDB.getInstance().getConnection();
        try {
            PreparedStatement stmt = c.prepareStatement(sqlquery);
            ResultSet rs = stmt.executeQuery();
            List<T> list = new ArrayList<>();
            while (rs.next()) {
                list.add(initObject(rs));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    protected abstract T initObject(ResultSet rs);

    private boolean jsp(Connection c, StringBuilder stringBuilder) {
        try {
            System.out.println(stringBuilder.toString());
            PreparedStatement stmt = c.prepareStatement(stringBuilder.toString());
            stmt.execute();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public String getTableName() {
        return tableName;
    }

    public List<String> getNomChampBdd() {
        return nomChampBdd;
    }
}

package fil.coo.datamapper;

import fil.coo.domain.PossedeDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PossedeMapper extends DataMapper<PossedeDAO> {
    public PossedeMapper() {
        super(new PossedeDAO());
    }


    @Override
    protected PossedeDAO initObject(ResultSet rs) {
        PossedeDAO p = new PossedeDAO();
        try {
            p.setIdUtilisateur(rs.getInt("id_utilisateur"));
            p.setIdVehicule(rs.getInt("id_vehicule"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return p;
    }
}

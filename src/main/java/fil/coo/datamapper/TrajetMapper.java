package fil.coo.datamapper;

import fil.coo.domain.TrajetDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrajetMapper extends DataMapper<TrajetDAO> {
    public TrajetMapper() {
        super(new TrajetDAO());
    }

    @Override
    protected TrajetDAO initObject(ResultSet rs) {
        TrajetDAO t = new TrajetDAO();
        try {
            t.setIdTrajet(rs.getInt("id_trajet"));
            t.setLieuDepart(rs.getString("lieu_depart"));
            t.setLieuArrive(rs.getString("lieu_arrive"));
            t.setDateDepart(rs.getString("date_depart"));
            t.setTarif(rs.getInt("tarif"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return t;
    }
}

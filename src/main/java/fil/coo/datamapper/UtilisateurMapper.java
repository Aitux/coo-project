package fil.coo.datamapper;

import fil.coo.domain.UtilisateurDAO;

import java.sql.ResultSet;
import java.sql.SQLException;


public class UtilisateurMapper extends DataMapper<UtilisateurDAO> {


    public UtilisateurMapper() {
        super(new UtilisateurDAO());
    }


    @Override
    protected UtilisateurDAO initObject(ResultSet rs) {

        UtilisateurDAO u = new UtilisateurDAO();
        try {
            System.out.println(rs.getInt("id_utilisateur"));
            u.setIdUtilisateur(rs.getInt("id_utilisateur"));
            u.setLogin(rs.getString("login"));
            u.setAge(rs.getInt("age"));
            u.setNom(rs.getString("nom"));
            u.setPrenom(rs.getString("prenom"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return u;
    }
}

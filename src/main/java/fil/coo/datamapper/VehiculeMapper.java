package fil.coo.datamapper;

import fil.coo.ConnexionDB;
import fil.coo.domain.VehiculeDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculeMapper extends DataMapper<VehiculeDAO> {

    public VehiculeMapper() {
        super(new VehiculeDAO());
    }

    @Override
    public VehiculeDAO readOne(String sqlquery) {
        Connection c = ConnexionDB.getInstance().getConnection();
        try {
            PreparedStatement stmt = c.prepareStatement(sqlquery);
            ResultSet rs = stmt.executeQuery();

            VehiculeDAO v = new VehiculeDAO();
            if(rs.next()) {
                v.setCapacite(rs.getInt("capacite"));
                v.setIdVehicule(rs.getInt("id_vehicule"));
                v.setMarque(rs.getString("marque"));
            }
            return v;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    protected VehiculeDAO initObject(ResultSet rs) {
        VehiculeDAO v = new VehiculeDAO();
        try {
            v.setCapacite(rs.getInt("capacite"));
            v.setIdVehicule(rs.getInt("id_vehicule"));
            v.setMarque(rs.getString("marque"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return v;
    }
}

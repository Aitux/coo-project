package fil.coo.datamapper;

import fil.coo.domain.VoyageurDAO;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VoyageurMapper extends DataMapper<VoyageurDAO> {

    public VoyageurMapper() {
        super(new VoyageurDAO());
    }

    @Override
    protected VoyageurDAO initObject(ResultSet rs) {
        VoyageurDAO v = new VoyageurDAO();
        try{
            v.setIdTrajet(rs.getInt("id_trajet"));
            v.setIdUtilisateur(rs.getInt("id_utilisateur"));
        }catch (SQLException e){
            e.printStackTrace();
        }

        return v;
    }
}

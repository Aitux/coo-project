package fil.coo.domain;

public class ConducteurDAO {
    private int idUtilisateur;
    private int idTrajet;

    public ConducteurDAO(){
    }

    public ConducteurDAO(int idUtilisateur, int idTrajet) {
        this.idUtilisateur = idUtilisateur;
        this.idTrajet = idTrajet;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(int idTrajet) {
        this.idTrajet = idTrajet;
    }
}

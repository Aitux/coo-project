package fil.coo.domain;

public class PossedeDAO {
    private int idUtilisateur;
    private int idVehicule;

    public PossedeDAO() {
    }

    public PossedeDAO(int idUtilisateur, int idVehicule) {
        this.idUtilisateur = idUtilisateur;
        this.idVehicule = idVehicule;
    }

    @Override
    public String toString() {
        return "PossedeDAO{" +
                "idUtilisateur=" + idUtilisateur +
                ", idVehicule=" + idVehicule +
                '}';
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdVehicule() {
        return idVehicule;
    }

    public void setIdVehicule(int idVehicule) {
        this.idVehicule = idVehicule;
    }
}

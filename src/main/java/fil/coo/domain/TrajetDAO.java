package fil.coo.domain;

import java.util.List;

public class TrajetDAO {
    private String lieuDepart;
    private String lieuArrive;
    private String dateDepart;
    private int tarif;
    private int idTrajet;



    public TrajetDAO(){}

    public TrajetDAO(String lieuDepart, String lieuArrive, String dateDepart, int tarif, int idTrajet) {
        this.lieuDepart = lieuDepart;
        this.lieuArrive = lieuArrive;
        this.dateDepart = dateDepart;
        this.tarif = tarif;
        this.idTrajet = idTrajet;
    }

    public TrajetDAO(String lieuDepart, String lieuArrive, String dateDepart, int tarif) {
        this.lieuDepart = lieuDepart;
        this.lieuArrive = lieuArrive;
        this.dateDepart = dateDepart;
        this.tarif = tarif;
    }

    public String getLieuDepart() {
        return lieuDepart;
    }

    public void setLieuDepart(String lieuDepart) {
        this.lieuDepart = lieuDepart;
    }

    public String getLieuArrive() {
        return lieuArrive;
    }

    public void setLieuArrive(String lieuArrive) {
        this.lieuArrive = lieuArrive;
    }

    public String getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }

    public int getTarif() {
        return tarif;
    }

    public void setTarif(int tarif) {
        this.tarif = tarif;
    }

    public int getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(int idTrajet) {
        this.idTrajet = idTrajet;
    }
}

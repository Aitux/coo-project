package fil.coo.domain;

public class UtilisateurDAO {

    private String nom;
    private String prenom;
    private int age;
    private String login;
    private int idUtilisateur;

    public UtilisateurDAO() {
    }

    public UtilisateurDAO(String nom, String prenom, int age, String login, int idUtilisateur) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
        this.login = login;
        this.idUtilisateur = idUtilisateur;
    }

    public UtilisateurDAO(UtilisateurDAO ud) {
        this.nom = ud.getNom();
        this.prenom = ud.getPrenom();
        this.age = ud.getAge();
        this.login = ud.getLogin();
        this.idUtilisateur = ud.getIdUtilisateur();
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

}

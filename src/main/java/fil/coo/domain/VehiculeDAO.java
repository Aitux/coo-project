package fil.coo.domain;

public class VehiculeDAO {

    private int capacite;
    private int idVehicule;
    private String marque;

    public VehiculeDAO() {
    }

    public VehiculeDAO(int capacite, int idVehicule, String marque) {
        this.capacite = capacite;
        this.idVehicule = idVehicule;
        this.marque = marque;
    }

    public int getCapacite() {
        return capacite;
    }

    public void setCapacite(int capacite) {
        this.capacite = capacite;
    }

    public int getIdVehicule() {
        return idVehicule;
    }

    public void setIdVehicule(int idVehicule) {
        this.idVehicule = idVehicule;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }
}

package fil.coo.domain;

public class VoyageurDAO {
    private int idUtilisateur;
    private int idTrajet;

    public VoyageurDAO(){

    }

    public VoyageurDAO(int idUtilisateur, int idTrajet) {
        this.idUtilisateur = idUtilisateur;
        this.idTrajet = idTrajet;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdTrajet() {
        return idTrajet;
    }

    public void setIdTrajet(int idTrajet) {
        this.idTrajet = idTrajet;
    }
}
